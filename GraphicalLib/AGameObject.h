#ifndef __A_GAME_OBJECT_H__
# define __A_GAME_OBJECT_H__

# include <SFML/Graphics.hpp>
# include <memory>

namespace RType
{
	namespace Graphical
	{
		enum class Event;

		class AGameObject
		{
		public:
			AGameObject();
			AGameObject(size_t);
			AGameObject(size_t, const std::shared_ptr<sf::Texture>&);
			AGameObject(size_t, const std::shared_ptr<sf::Texture>&, float);
			virtual ~AGameObject();

			virtual const sf::Sprite&	getDrawable() const;
			virtual const sf::Vector2f&	getPos() const;
			virtual size_t				getID() const;

			virtual bool				update(sf::Vector2u, Event, const sf::Time&);
			virtual void				setPos(const sf::Vector2f&);

		protected:
			sf::Sprite						_Drawable;
			std::shared_ptr<sf::Texture>	_Texture;
			float							_Speed;
			size_t							_ID;
		};
	}
}

#endif // !__A_GAME_OBJECT_H__
