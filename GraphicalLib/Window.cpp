#include "Window.h"

namespace RType
{
	namespace Graphical
	{
		Window::Window() : _Window(sf::VideoMode(1280, 1020), "RType",
			sf::Style::Close), _Event()
		{
			this->_Window.setFramerateLimit(60);
		}

		Window::~Window()
		{
		}

		/////////////////////////////

		bool	Window::isOpen() const
		{
			return this->_Window.isOpen();
		}

		Event	Window::getEvent() const
		{
			switch (this->_Event.type)
			{
			case sf::Event::Closed:
				return Event::Quit;
			case sf::Event::KeyPressed:
				switch (this->_Event.key.code)
				{
				case sf::Keyboard::Escape:
					return Event::KeyEscape;
				case sf::Keyboard::Right:
					return Event::KeyRight;
				case sf::Keyboard::Left:
					return Event::KeyLeft;
				case sf::Keyboard::Down:
					return Event::KeyDown;
				case sf::Keyboard::Up:
					return Event::KeyUp;
				case sf::Keyboard::Space:
					return Event::KeySpace;
				}
			default:
				return Event::None;
			}
		}

		const sf::Vector2u&	Window::getSize() const
		{
			return this->_Window.getSize();
		}

		bool	Window::update()
		{
			return this->_Window.pollEvent(this->_Event);
		}

		void	Window::clear()
		{
			this->_Window.clear(sf::Color::Black);
		}

		void	Window::draw(const AGameObject& object)
		{
			this->_Window.draw(object.getDrawable());
		}

		void	Window::draw(const std::vector<std::unique_ptr<AGameObject>>& obj)
		{
			for (auto &it : obj)
			{
				this->_Window.draw(it->getDrawable());
			}
		}

		void	Window::draw(const sf::Sprite& sprite)
		{
			this->_Window.draw(sprite);
		}

		void	Window::display()
		{
			this->_Window.display();
		}
	}
}