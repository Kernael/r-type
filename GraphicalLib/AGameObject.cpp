#include <iostream>
#include "Window.h"

namespace RType
{
	namespace Graphical
	{
		AGameObject::AGameObject() : _Drawable(), _Texture(), _Speed(0.f), _ID(0)
		{
		}

		AGameObject::AGameObject(size_t id) : _Drawable(), _Texture(), _Speed(0.f), _ID(id)
		{
		}

		AGameObject::AGameObject(size_t id, const std::shared_ptr<sf::Texture>& texture)
			: _Drawable(), _Texture(texture), _Speed(0.f), _ID(id)
		{
		}

		AGameObject::AGameObject(size_t id, const std::shared_ptr<sf::Texture>& texture, float speed)
			: _Drawable(), _Texture(texture), _Speed(speed), _ID(id)
		{
		}

		AGameObject::~AGameObject()
		{
		}

		/////////////////////////////

		bool	AGameObject::update(sf::Vector2u, RType::Graphical::Event,
			const sf::Time&)
		{
			return false;
		}

		void	AGameObject::setPos(const sf::Vector2f& pos)
		{
			this->_Drawable.setPosition(pos);
		}

		const sf::Sprite&	AGameObject::getDrawable() const
		{
			return this->_Drawable;
		}

		const sf::Vector2f&	AGameObject::getPos() const
		{
			return this->_Drawable.getPosition();
		}

		size_t	AGameObject::getID() const
		{
			return this->_ID;
		}
	}
}