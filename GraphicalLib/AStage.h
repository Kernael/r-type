#ifndef __A_STAGE_H__
# define __A_STAGE_H__

# include <vector>
# include <memory>
# include <map>
# include "AGameObject.h"
# include "Window.h"

namespace RType
{
	namespace Graphical
	{
		typedef std::unique_ptr<AGameObject>	ObjectPtr;
		typedef std::shared_ptr<Window>			WindowPtr;
		typedef std::shared_ptr<sf::Texture>	TexturePtr;

		class AStage
		{
		public:
			AStage();
			AStage(const WindowPtr&);
			virtual ~AStage();

			virtual void	run() = 0;

		protected:
			sf::Clock								_Clock;
			std::vector<ObjectPtr>					_Objects;
			std::map<std::string, TexturePtr>		_Textures;
			WindowPtr								_Window;
		};
	}
}

#endif // !__A_STAGE_H__
