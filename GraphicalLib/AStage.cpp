#include "AStage.h"

namespace RType
{
	namespace Graphical
	{
		AStage::AStage() : _Window(), _Objects(), _Clock(), _Textures()
		{
		}

		AStage::AStage(const WindowPtr& win) : _Window(win), _Objects(), _Clock(),
			_Textures()
		{
		}

		AStage::~AStage()
		{
		}
	}
}