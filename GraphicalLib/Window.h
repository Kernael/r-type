#ifndef __WINDOW_H__
# define __WINDOW_H__

# include <memory>
# include <SFML/Graphics.hpp>
# include "AGameObject.h"

namespace RType
{
	namespace Graphical
	{
		enum class Event
		{
			None, Quit, KeyEscape, KeyRight, KeyLeft, KeyDown, KeyUp, KeySpace
		};

		class Window
		{
		public:
			Window();
			~Window();

			bool				isOpen() const;
			Event				getEvent() const;
			const sf::Vector2u&	getSize() const;

			bool	update();
			void	clear();
			void	draw(const AGameObject&);
			void	draw(const std::vector<std::unique_ptr<AGameObject>>&);
			void	draw(const sf::Sprite&);
			void	display();

		private:
			sf::RenderWindow	_Window;
			sf::Event			_Event;
		};
	}
}

#endif // !__WINDOW_H__
