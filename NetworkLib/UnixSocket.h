#ifndef __UNIX_SOCKET_H__
# define __UNIX_SOCKET_H__

# include "ISocket.h"

namespace RType
{
	namespace Network
	{
		class UnixSocket : public ISocket
		{
		public:
			UnixSocket();
			virtual ~UnixSocket();

		private:
		};
	}
}

#endif // !__UNIX_SOCKET_H__
