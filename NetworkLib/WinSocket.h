#ifndef __WIN_SOCKET_H__
# define __WIN_SOCKET_H__

# include "ISocket.h"

namespace RType
{
	namespace Network
	{
		class WinSocket : public ISocket
		{
		public:
			WinSocket();
			virtual ~WinSocket();

		private:
		};
	}
}

#endif // !__WIN_SOCKET_H__
