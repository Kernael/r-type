#ifndef __I_LIB_LOADER_H__
# define __I_LIB_LOADER_H__

template<typename T>
class ILibLoader
{
public:
	virtual ~ILibLoader();

	virtual T*		getInstance(const std::string&) = 0;
	virtual void	unload() = 0;
};

#endif // !__I_LIB_LOADER_H__
