#include "Client.h"
#include "Game.h"

namespace RType
{
	Client::Client()
	{
	}

	Client::~Client()
	{
	}

	/////////////////////////////

	void	Client::run()
	{
		RType::Graphical::WindowPtr	window(new RType::Graphical::Window);
		std::vector<size_t>			players = { 1 }; // get players list from server
		size_t						selfID = 1; // get self id from serv

		Game	game(window, selfID);

		game.run();
	}
}