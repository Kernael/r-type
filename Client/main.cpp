#include <iostream>
#include "Client.h"

int		main()
{
	try
	{
		RType::Client	client;

		// se connecter au serveur

		client.run();
	}
	catch (const std::exception& e)
	{
		std::cerr << "Exception catched : " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Unknown error" << std::endl;
	}

	std::cin.get();
	return 0;
}