#ifndef __MISSILE_H__
# define __MISSILE_H__

# include "Window.h"
# include <memory>

class Missile : public RType::Graphical::AGameObject
{
public:
	Missile();
	Missile(size_t);
	Missile(size_t, const std::shared_ptr<sf::Texture>&);
	virtual ~Missile();

	virtual bool update(sf::Vector2u, RType::Graphical::Event, const sf::Time&);

private:
};

#endif // !__MISSILE_H__