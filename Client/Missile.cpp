#include "Missile.h"

Missile::Missile() : AGameObject()
{
}

Missile::Missile(size_t id) : AGameObject(id)
{
}

Missile::Missile(size_t id, const std::shared_ptr<sf::Texture>& texture)
	: AGameObject(id, texture, 750.f)
{
	this->_Drawable.setTexture(*this->_Texture);
}

Missile::~Missile()
{
}

/////////////////////////////

bool	Missile::update(sf::Vector2u winSize, RType::Graphical::Event,
	const sf::Time& elapsed)
{
	this->_Drawable.move(this->_Speed * elapsed.asSeconds(), 0.f);

	if (this->_Drawable.getPosition().x > winSize.x)
	{
		return false;
	}

	return true;
}