#include "Player.h"

Player::Player() : AGameObject(), _LastKeyPressed()
{
}

Player::Player(size_t id) : AGameObject(id), _LastKeyPressed()
{
}

Player::Player(size_t id, const std::shared_ptr<sf::Texture>& texture)
	: AGameObject(id, texture, 250.f)
{
	this->_Drawable.setTexture(*this->_Texture);
	this->_Drawable.setOrigin(16, 9);
	this->_Drawable.move(200, 100);
	this->_Drawable.scale(sf::Vector2f(2.f, 2.f));
}

Player::~Player()
{
}

/////////////////////////////

bool	Player::update(sf::Vector2u winSize, RType::Graphical::Event event,
	const sf::Time& elapsed)
{
	if (event == RType::Graphical::Event::KeyDown ||
		event == RType::Graphical::Event::KeyUp ||
		event == RType::Graphical::Event::KeyRight ||
		event == RType::Graphical::Event::KeyLeft)
	{
		this->_LastKeyPressed = event;
	}

	switch (this->_LastKeyPressed)
	{
	case RType::Graphical::Event::KeyUp:
		if (this->_Drawable.getPosition().y > 0)
		{
			this->_Drawable.move(0, -1 * (this->_Speed * elapsed.asSeconds()));
		}
		break;
	case RType::Graphical::Event::KeyDown:
		if (this->_Drawable.getPosition().y < winSize.y)
		{
			this->_Drawable.move(0, 1 * (this->_Speed * elapsed.asSeconds()));
		}
		break;
	case RType::Graphical::Event::KeyLeft:
		if (this->_Drawable.getPosition().x > 0)
		{
			this->_Drawable.move(-1 * (this->_Speed * elapsed.asSeconds()), 0);
		}
		break;
	case RType::Graphical::Event::KeyRight:
		if (this->_Drawable.getPosition().x < winSize.x)
		{
			this->_Drawable.move(this->_Speed * elapsed.asSeconds(), 0);
		}
		break;
	default:
		break;
	}
	return true;
}