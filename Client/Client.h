#ifndef __CLIENT_H__
# define __CLIENT_H__

# include <memory>
# include "Window.h"

namespace RType
{
	class Client
	{
	public:
		Client();
		~Client();

		void	run();

	private:
	};
}

#endif // !__CLIENT_H__
