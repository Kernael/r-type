#include <iostream>
#include "Game.h"
#include "Player.h"
#include "Missile.h"

Game::Game() : AStage()
{
}

Game::Game(const RType::Graphical::WindowPtr& win)
	: AStage(win), _playerID(0), _playerPos(0.f, 0.f), _BgLeft(), _BgRight()
{
}

Game::Game(const RType::Graphical::WindowPtr& win, size_t id)
	: AStage(win), _playerID(id), _playerPos(0.f, 0.f), _BgLeft(), _BgRight()
{
	RType::Graphical::TexturePtr	bgTexture(new sf::Texture);

	if (!bgTexture->loadFromFile("../sprites/starfield.jpg"))
	{
		std::cerr << "Could not open Background texture file" << std::endl;
		throw std::exception();
	}

	bgTexture->setSmooth(true);
	bgTexture->setRepeated(true);

	this->_Textures.insert(std::pair<std::string, RType::Graphical::TexturePtr>(
		"background", bgTexture));

	this->_BgLeft.setTexture(*this->_Textures.at("background").get());
	this->_BgRight.setTexture(*this->_Textures.at("background").get());

	this->_BgLeft.setPosition(0.f, 0.f);
	this->_BgRight.setPosition(this->_BgLeft.getGlobalBounds().width, 0.f);

	RType::Graphical::TexturePtr	shipTexture(new sf::Texture);

	if (!shipTexture->loadFromFile("../sprites/r-typesheet1.gif",
		sf::IntRect(134, 0, 32, 18)))
	{
		std::cerr << "Could not open Player texture file" << std::endl;
		throw std::exception();
	}

	shipTexture->setSmooth(true);
	shipTexture->setRepeated(false);

	this->_Textures.insert(std::pair<std::string, RType::Graphical::TexturePtr>(
		"ship", shipTexture));

	this->_Objects.push_back(std::unique_ptr<RType::Graphical::AGameObject>(
		new Player(id, shipTexture)));

	RType::Graphical::TexturePtr	missileTexture(new sf::Texture);

	if (!missileTexture->loadFromFile("../sprites/r-typesheet1.gif",
		sf::IntRect(210, 150, 55, 18)))
	{
		std::cerr << "Could not open missile texture file" << std::endl;
		throw std::exception();
	}

	missileTexture->setSmooth(true);
	missileTexture->setRepeated(false);

	this->_Textures.insert(std::pair<std::string, RType::Graphical::TexturePtr>(
		"missile", missileTexture));
}

Game::~Game()
{
}

/////////////////////////////

void	Game::run()
{
	while (this->_Window->isOpen())
	{
		sf::Time	elapsed = this->_Clock.restart();

		std::cout << 1 / elapsed.asSeconds() << std::endl;

		this->_Window->update();

		RType::Graphical::Event	event(this->_Window->getEvent());

		switch (event)
		{
		case RType::Graphical::Event::Quit:
			std::cout << "Exiting [quit]" << std::endl;
			return;
		case RType::Graphical::Event::KeyEscape:
			std::cout << "Exiting [esc]" << std::endl;
			return;
		case RType::Graphical::Event::KeySpace:
			this->createNewMissile();
		default:
			break;
		}

		for (std::vector<RType::Graphical::ObjectPtr>::const_iterator
			it = this->_Objects.begin(); it < this->_Objects.end();)
		{
			if ((*it)->getID() == this->_playerID)
			{
				this->_playerPos = (*it)->getPos();
			}

			if (!(*it)->update(this->_Window->getSize(), event, elapsed))
			{
				it = this->_Objects.erase(it);
			}
			else
			{
				it++;
			}
		}

		this->updateBackgrounds(elapsed);

		this->_Window->clear();

		this->_Window->draw(this->_BgLeft);
		this->_Window->draw(this->_BgRight);
		this->_Window->draw(this->_Objects);
		this->_Window->display();
	}
}

/////////////////////////////

void	Game::createNewMissile()
{
	this->_Objects.push_back(std::unique_ptr<RType::Graphical::AGameObject>(
		new Missile(0, this->_Textures.at("missile"))));
	this->_Objects.back()->setPos(this->_playerPos);
}

void	Game::updateBackgrounds(const sf::Time& elapsed)
{
	float		width = this->_BgLeft.getGlobalBounds().width;

	this->_BgLeft.move(-1 * (250.f * elapsed.asSeconds()), 0.f);
	this->_BgRight.move(-1 * (250.f * elapsed.asSeconds()), 0.f);

	if (this->_BgLeft.getPosition().x < (-1 * width))
	{
		this->_BgLeft.setPosition(width, 0.f);
	}
	if (this->_BgRight.getPosition().x < (-1 * width))
	{
		this->_BgRight.setPosition(width, 0.f);
	}
}