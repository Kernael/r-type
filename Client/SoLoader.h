#ifndef __SO_LOADER_H__
# define __SO_LOADER_H__

# include "ILibLoader.h"

template<typename T>
class SoLoader : public ILibLoader
{
public:
	SoLoader();
	virtual ~SoLoader();

	virtual T*		getInstance(const std::string&);
	virtual void	unload();

private:
};

#endif // !__SO_LOADER_H__
