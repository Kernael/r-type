#ifndef __PLAYER_H__
# define __PLAYER_H__

# include "Window.h"
# include <memory>

class Player : public RType::Graphical::AGameObject
{
public:
	Player();
	Player(size_t);
	Player(size_t, const std::shared_ptr<sf::Texture>&);
	virtual ~Player();

	virtual bool update(sf::Vector2u, RType::Graphical::Event, const sf::Time&);

private:
	RType::Graphical::Event	_LastKeyPressed;
};

#endif // !__PLAYER_H__