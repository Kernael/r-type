#ifndef __DLL_LOADER_H__
# define __DLL_LOADER_H__

# include "ILibLoader.h"

template<typename T>
class DllLoader : public ILibLoader
{
public:
	DllLoader();
	virtual ~DllLoader();

	virtual T*		getInstance(const std::string&);
	virtual void	unload();

private:
};

#endif // !__DLL_LOADER_H__
