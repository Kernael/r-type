#ifndef __GAME_H__
# define __GAME_H__

# include "AStage.h"
# include "Window.h"

class Game : public RType::Graphical::AStage
{
public:
	Game();
	Game(const RType::Graphical::WindowPtr&);
	Game(const RType::Graphical::WindowPtr&, size_t);
	virtual ~Game();

	virtual void	run();

	void	createNewMissile();
	void	updateBackgrounds(const sf::Time&);

private:
	size_t			_playerID;
	sf::Vector2f	_playerPos;
	sf::Sprite		_BgLeft;
	sf::Sprite		_BgRight;
};

#endif // !__GAME_H__
