#ifndef __SERVER_H__
# define __SERVER_H__

# include <memory>
# include "ISocket.h"

namespace RType
{
	class Server
	{
	public:
		Server();
		~Server();

	private:
		std::unique_ptr<Network::ISocket>	_Socket;
	};
}

#endif // !__SERVER_H__
