#include <iostream>
#include "Server.h"

int		main()
{
	try
	{
		std::cout << "Testing server" << std::endl;

		RType::Server	server;
	}
	catch (const std::exception& e)
	{
		std::cerr << "Exception catched : " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Unknown error" << std::endl;
	}

	std::cin.get();
	return 0;
}